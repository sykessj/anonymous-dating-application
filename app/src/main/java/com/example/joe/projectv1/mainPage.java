package com.example.joe.projectv1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

public class mainPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int testingSave = 15;
        String testingSave2 = "Testing the save method";
        //Creating the sharedpreferences object
        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
        //get the editor
        String nameText = sharedPref.getString("Name", "");
        String userNameText = sharedPref.getString("userName", "");
        String passwordText = sharedPref.getString("password", "");
        String genderText = sharedPref.getString("gender", "");
        String seekingText = sharedPref.getString("seeking", "");
        String ageText = sharedPref.getString("age", "");


        Log.d("test1", "This should print to log cat");
        String test = "bla ba";
        Log.d("test2", "Name: " + nameText);
        Log.d("test3", "Username: " + userNameText);
        Log.d("test4", "Password: " + passwordText);
        Log.d("test5", "Gender: " + genderText);
        Log.d("test6", "Seeking: " + seekingText);
        Log.d("test7", "Age: " + ageText);





        ((TextView) findViewById(R.id.user_name)).setText("" + nameText);


        //get the entire profile

        int quiz1Score = sharedPref.getInt("Quiz1_score", 0);
        int quiz2Score = sharedPref.getInt("Quiz2_score", 0);
        int quiz3Score = sharedPref.getInt("Quiz3_score", 0);
        Log.d("profile2", "Name: " + nameText);
        Log.d("profile3", "Username: " + userNameText);
        Log.d("profile4", "Password: " + passwordText);
        Log.d("profile5", "Gender: " + genderText);
        Log.d("profile6", "Seeking: " + seekingText);
        Log.d("profile7", "Age: " + ageText);
        Log.d("profile8", "Quiz 1 Score: " + quiz1Score);
        Log.d("profile9", "Quiz 2 Score " + quiz2Score);
        Log.d("profile10", "Quiz 3 Score " + quiz3Score);




    }

    public void onQuizzes(View view){



        Intent intent = new Intent(this, quiz_list.class);
        startActivity(intent);
    }

    public void onEditProfile(View view){
        Intent intent = new Intent(this, editProfile.class);
        startActivity(intent);
    }

    public void onSettings(View view){

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
        //get the editor
        String number = "3";
        String personIdentifier = "Person" + number;
        int personScore = sharedPref.getInt(personIdentifier, 0);
        Log.d("person", "Person Score: " + personScore);
        String displayText = "Person Score: " + personScore;
        Toast toast = Toast.makeText(this, displayText, Toast.LENGTH_SHORT);
       toast.show();










       /* try {
            SQLiteOpenHelper databaseHelper = new databaseHelper(this);
            SQLiteDatabase db = databaseHelper.getReadableDatabase();

            Cursor cursor = db.query("USER_DETAILS",
                    new String[]{"USERNAME", "PASSWORD"},
                    "_id = ?",
                    new String[]{Integer.toString(1)},
                    null, null, null);

            if (cursor.moveToFirst()) {
                String username = cursor.getString(0);
                Toast toast = Toast.makeText(this, "This has worked a little bit", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(this, "Didnt find anything", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        catch(SQLiteException e){
            Toast toast = Toast.makeText(this, "Database Unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }*/


    }

    public void onMatches(View view){

        Intent intent = new Intent(this, matches.class);
        startActivity(intent);


    }

}
