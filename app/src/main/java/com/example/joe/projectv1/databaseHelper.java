package com.example.joe.projectv1;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Joe on 31/03/2017.
 */
public class databaseHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "projectDB"; //name of database
    private static final int DB_VERSION = 1; //Version of the database

    databaseHelper(Context context) {

        //Calling the constructor of the SQLite open helper class
        super(context, DB_NAME, null, DB_VERSION);
        //Database not yet created, just initialised

    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE USER_DETAILS (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "USERNAME TEXT, " +
                "PASSWORD TEXT, " +
                "QUIZ1_SCORE INTEGER, " +
                "QUIZ2_SCORE INTEGER, " +
                "QUIZ3_SCORE INTEGER, " +
                "QUIZ4_SCORE INTEGER);");

        addUser(db, "usernameTest", "passwordTest");
        addUser(db, "second", "secondPass");


    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private static void addUser(SQLiteDatabase db, String username, String password ){

        ContentValues userValues = new ContentValues();
        userValues.put("USERNAME", username);
        userValues.put("PASSWORD", password);
        userValues.put("Q1", 0);
        userValues.put("Q2", 0);
        userValues.put("Q3", 0);
        userValues.put("Q4", 0);
        db.insert("USER_DETAILS", null, userValues);
    }

    private static void deleteDrink(SQLiteDatabase db, String table, String whereClause, String[] whereArgs){
        db.delete(table, whereClause, whereArgs);
    }
}
