package com.example.joe.projectv1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
        //get the editor
        SharedPreferences.Editor editor = sharedPref.edit();
        //Put the value to be saved

        //Person 1
        editor.putString("Person1Name", "Jenny");
        editor.putInt("Person1Age", 25);
        editor.putInt("Person1Score1", 10);
        editor.putInt("Person1Score2", 20);
        editor.putInt("Person1Score3", 30);
        editor.putInt("Person1Overall", 50);

        editor.putString("Person2Name", "Sarah");
        editor.putInt("Person2Age", 25);
        editor.putInt("Person2Score1", 10);
        editor.putInt("Person2Score2", 20);
        editor.putInt("Person2Score3", 30);
        editor.putInt("Person2Overall", 100);

        editor.putString("Person3Name", "Melissa");
        editor.putInt("Person3Age", 25);
        editor.putInt("Person3Score1", 10);
        editor.putInt("Person3Score2", 20);
        editor.putInt("Person3Score3", 30);
        editor.putInt("Person3Overall", 200);

        editor.putString("Person4Name", "Jan");
        editor.putInt("Person4Age", 25);
        editor.putInt("Person4Score1", 10);
        editor.putInt("Person4Score2", 20);
        editor.putInt("Person4Score3", 30);
        editor.putInt("Person4Overall", 300);

        editor.putString("Person5Name", "Wendy");
        editor.putInt("Person5Age", 25);
        editor.putInt("Person5Score1", 10);
        editor.putInt("Person5Score2", 20);
        editor.putInt("Person5Score3", 30);
        editor.putInt("Person5Overall", 405);

        editor.commit();


    }

    public void onSignIn(View view){
        Intent intent = new Intent(this, loggingInActivity.class);
        startActivity(intent);
    }

    public void onRegister(View view){
        Intent intent = new Intent(this, registerPage.class);
        startActivity(intent);
    }
}
