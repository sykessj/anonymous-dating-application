package com.example.joe.projectv1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class quiz_list extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);



        //Create the onItemClickListener
        AdapterView.OnItemClickListener itemClickListener =
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> listView,
                                            View v,
                                            int position,
                                            long id) {

                        String quizSelect = "quiz1";
                        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
                        //get the editor
                        SharedPreferences.Editor editing = sharedPref.edit();


                        if (position == 0) {
                            quizSelect = "quiz1";
                            editing.putString("quizSelect", quizSelect);
                            editing.commit();


                            Intent intent = new Intent(quiz_list.this,
                                    quiz_activity.class);
                            startActivity(intent);
                        }
                        else if (position == 1){
                            quizSelect = "quiz2";
                            Log.d("quizTest2", "QuizSelect (at quiz list - before): " + quizSelect);
                            editing.putString("quizSelect", quizSelect);
                            editing.commit();

                            Intent intent = new Intent(quiz_list.this,
                                    quiz_activity.class);
                            startActivity(intent);
                        }

                        else if (position == 2){
                            quizSelect = "quiz3";
                            Log.d("quizTest2", "QuizSelect (at quiz list - before): " + quizSelect);
                            editing.putString("quizSelect", quizSelect);
                            editing.commit();

                            Intent intent = new Intent(quiz_list.this,
                                    quiz_activity.class);
                            startActivity(intent);
                        }


                    }
                };
        //Add the listener to the list view
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(itemClickListener);
    }
}
