package com.example.joe.projectv1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class loggingInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging_in);
    }

    public void onLogIn(View view){
        final EditText userNameAttempt1 = ((EditText)findViewById(R.id.userNameAttemptText));
        final EditText passwordAttempt1 = ((EditText)findViewById(R.id.passwordAttemptText));

        String UserNameAttempt = userNameAttempt1.getText().toString();
        String PasswordAttempt = passwordAttempt1.getText().toString();

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
        String officialUserName = sharedPref.getString("userName", "");
        String officialPassword = sharedPref.getString("password", "");

        Log.d("userNameTest", "User name entered is: " + UserNameAttempt);
        Log.d("passwordTest", "Password entered is: " + PasswordAttempt);
        Log.d("officialPassTest", "Official User Name: " + officialUserName);
        Log.d("officalPassTest2", "Official Password: " + officialPassword);

        if(officialUserName.equals(UserNameAttempt) && officialPassword.equals(PasswordAttempt)){
            Intent intent = new Intent(this, mainPage.class);
            startActivity(intent);
        }
        else{
            Context context = getApplicationContext();
            CharSequence text = "Username or password is incorrect! Please try again";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    public void onCancel(View view){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
}
