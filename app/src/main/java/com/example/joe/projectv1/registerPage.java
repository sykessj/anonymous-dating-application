package com.example.joe.projectv1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class registerPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_pag);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




    }

    public void onSuccessfulRegister(View view){

        boolean problem = false;

        final EditText edit = ((EditText)findViewById(R.id.nameText));
        final EditText edit2 = ((EditText)findViewById(R.id.userNameText));
        final EditText edit3 = ((EditText)findViewById(R.id.passwordText));
        final Spinner edit4 = ((Spinner)findViewById(R.id.genderText));
        final Spinner edit5 = ((Spinner)findViewById(R.id.seekingText));
        final Spinner edit6 = ((Spinner)findViewById(R.id.ageText));

        String nameText = edit.getText().toString();
        String userNameText = edit2.getText().toString();
        String passwordText = edit3.getText().toString();
        String genderText = edit4.getSelectedItem().toString();
        String seekingText = edit5.getSelectedItem().toString();
        String ageText = edit6.getSelectedItem().toString();

        int genderPos = edit4.getSelectedItemPosition();
        int seekingPos = edit4.getSelectedItemPosition();
        int agePos = edit4.getSelectedItemPosition();


        if (nameText.equals("")) {
            problem = true;
        }
        if (userNameText.equals("")) {
            problem = true;
        }
        if (passwordText.equals("")) {
            problem = true;
        }


        if (problem == false) {

            SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
            //get the editor
            SharedPreferences.Editor editor = sharedPref.edit();
            //Put the value to be saved
            editor.putString("Name", nameText);
            editor.putString("userName", userNameText);
            editor.putString("password", passwordText);
            editor.putString("gender", genderText);
            editor.putString("seeking", seekingText);
            editor.putString("age", ageText);
            editor.putInt("genderPos", genderPos);
            editor.putInt("seekingPos", seekingPos);
            editor.putInt("agePos", agePos);
            editor.commit();


            Intent intent = new Intent(this, mainPage.class);
            startActivity(intent);

        } else if (problem == true) {

            Context context = getApplicationContext();
            CharSequence text = "One or more fields not completed";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }



    }

}
