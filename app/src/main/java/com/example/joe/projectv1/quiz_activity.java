package com.example.joe.projectv1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class quiz_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_activity);

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);

        String quizSelect = "";

        quizSelect = sharedPref.getString("quizSelect", "");
        Log.d("quizTest", "quizSelect (after): " + quizSelect);



        if(quizSelect == "quiz1") {

            ((TextView) findViewById(R.id.questionView)).setText("What type of relationship are you looking for?");
            ((TextView) findViewById(R.id.answerView1)).setText("Short Term");
            ((TextView) findViewById(R.id.answerView2)).setText("Long Term");
            ((TextView) findViewById(R.id.answerView3)).setText("Friendship");
            ((TextView) findViewById(R.id.answerView4)).setText("None");
        }

        else if(quizSelect == "quiz2"){

            ((TextView) findViewById(R.id.questionView)).setText("What is more important in a partner?");
            ((TextView) findViewById(R.id.answerView1)).setText("Personality");
            ((TextView) findViewById(R.id.answerView2)).setText("Height");
            ((TextView) findViewById(R.id.answerView3)).setText("Looks");
            ((TextView) findViewById(R.id.answerView4)).setText("Money");

        }

        else if(quizSelect == "quiz3"){

            ((TextView) findViewById(R.id.questionView)).setText("How much would you usually spend at a restuarant?");
            ((TextView) findViewById(R.id.answerView1)).setText("less than £10");
            ((TextView) findViewById(R.id.answerView2)).setText("£11 - £17");
            ((TextView) findViewById(R.id.answerView3)).setText("£18 - £25");
            ((TextView) findViewById(R.id.answerView4)).setText("£26+");

        }


    }

    int counter = 0;
    int score = 0;

    public void nextQuestion(View view){

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);

        String quizSelect = sharedPref.getString("quizSelect", "");


        counter++;
        //Quiz 1
        if(quizSelect == "quiz1") {
            //QUESTION1 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 1)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 1)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 1)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 1)) {score = score - 100;}

            //QUESTION2 Answer scores
            if ((view.getId() == R.id.answerView1) && (counter == 2)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 2)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 2)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 2)) {score = score - 100;}

            //QUESTION3 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 3)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 3)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 3)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 3)) {score = score - 100;}

            //QUESTION3 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 4)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 4)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 4)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 4)) {score = score - 100;}

            //Questions
            if (counter == 1) {

                ((TextView) findViewById(R.id.questionView)).setText("Why do you want to start dating?");
                ((TextView) findViewById(R.id.answerView1)).setText("Meet people");
                ((TextView) findViewById(R.id.answerView2)).setText("Have fun");
                ((TextView) findViewById(R.id.answerView3)).setText("Not sure");
                ((TextView) findViewById(R.id.answerView4)).setText("I dont want to date");

            } else if (counter == 2) {

                ((TextView) findViewById(R.id.questionView)).setText("Are you currently single?");
                ((TextView) findViewById(R.id.answerView1)).setText("Yes");
                ((TextView) findViewById(R.id.answerView2)).setText("Its complicated");
                ((TextView) findViewById(R.id.answerView3)).setText("Now and then");
                ((TextView) findViewById(R.id.answerView4)).setText("No");

            } else if (counter == 3) {

                ((TextView) findViewById(R.id.questionView)).setText("Do you have friends of the opposite sex");
                ((TextView) findViewById(R.id.answerView1)).setText("Yes lots");
                ((TextView) findViewById(R.id.answerView2)).setText("A few");
                ((TextView) findViewById(R.id.answerView3)).setText("Barely any");
                ((TextView) findViewById(R.id.answerView4)).setText("No, not at all");

                //Saving the score
                //Creating the sharedpreferences object
                //SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
                //get the editor
                SharedPreferences.Editor editor = sharedPref.edit();
                //Put the value to be saved
                editor.putInt("Quiz1_score", score);
                //commit the edit to be saved
                editor.commit();

            } else if (counter == 4) {
                //End of quiz
                setContentView(R.layout.quiz_results);
                ((TextView) findViewById(R.id.scoreText)).setText("" + score);
            }
        }

        //Quiz 2
        else if(quizSelect == "quiz2"){

            //QUESTION1 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 1)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 1)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 1)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 1)) {score = score - 100;}

            //QUESTION2 Answer scores
            if ((view.getId() == R.id.answerView1) && (counter == 2)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 2)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 2)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 2)) {score = score - 100;}

            //QUESTION3 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 3)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 3)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 3)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 3)) {score = score - 100;}


            //Questions
            if (counter == 1) {

                ((TextView) findViewById(R.id.questionView)).setText("Are you nervous around the opposite sex?");
                ((TextView) findViewById(R.id.answerView1)).setText("Not at all");
                ((TextView) findViewById(R.id.answerView2)).setText("Sometimes");
                ((TextView) findViewById(R.id.answerView3)).setText("Most of the time");
                ((TextView) findViewById(R.id.answerView4)).setText("All of the time");
            }

            if (counter == 2) {

                ((TextView) findViewById(R.id.questionView)).setText("Do you think you are attractive?");
                ((TextView) findViewById(R.id.answerView1)).setText("Very");
                ((TextView) findViewById(R.id.answerView2)).setText("Relatively");
                ((TextView) findViewById(R.id.answerView3)).setText("Average");
                ((TextView) findViewById(R.id.answerView4)).setText("Not at all");

            }

            if (counter == 3) {

                ((TextView) findViewById(R.id.questionView)).setText("Do you have alot of friends?");
                ((TextView) findViewById(R.id.answerView1)).setText("Yes, lots");
                ((TextView) findViewById(R.id.answerView2)).setText("Above average");
                ((TextView) findViewById(R.id.answerView3)).setText("Average");
                ((TextView) findViewById(R.id.answerView4)).setText("Not so many");

                SharedPreferences.Editor editor = sharedPref.edit();
                //Put the value to be saved
                editor.putInt("Quiz2_score", score);
                //commit the edit to be saved
                editor.commit();

            }
            else if (counter == 4) {
                //End of quiz
                setContentView(R.layout.quiz_results);
                ((TextView) findViewById(R.id.scoreText)).setText("" + score);
            }


        }

        //Quiz 3
        else if(quizSelect == "quiz3"){

            //QUESTION1 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 1)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 1)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 1)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 1)) {score = score - 100;}

            //QUESTION2 Answer scores
            if ((view.getId() == R.id.answerView1) && (counter == 2)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 2)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 2)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 2)) {score = score - 100;}

            //QUESTION3 Answer Scores
            if ((view.getId() == R.id.answerView1) && (counter == 3)) {score = score + 100;}
            if ((view.getId() == R.id.answerView2) && (counter == 3)) {score = score + 50;}
            if ((view.getId() == R.id.answerView3) && (counter == 3)) {score = score;}
            if ((view.getId() == R.id.answerView4) && (counter == 3)) {score = score - 100;}


            //Questions
            if (counter == 1) {

                ((TextView) findViewById(R.id.questionView)).setText("What is your dream date?");
                ((TextView) findViewById(R.id.answerView1)).setText("Picnic");
                ((TextView) findViewById(R.id.answerView2)).setText("Little Cafe");
                ((TextView) findViewById(R.id.answerView3)).setText("Movie");
                ((TextView) findViewById(R.id.answerView4)).setText("5 Star Restaurant");
            }

            if (counter == 2) {

                ((TextView) findViewById(R.id.questionView)).setText("What would you prefer?");
                ((TextView) findViewById(R.id.answerView1)).setText("Happiness");
                ((TextView) findViewById(R.id.answerView2)).setText("A partner");
                ((TextView) findViewById(R.id.answerView3)).setText("Nice things");
                ((TextView) findViewById(R.id.answerView4)).setText("Money");

            }

            if (counter == 3) {

                ((TextView) findViewById(R.id.questionView)).setText("What's a perfect friday night?");
                ((TextView) findViewById(R.id.answerView1)).setText("Staying in");
                ((TextView) findViewById(R.id.answerView2)).setText("Drive in theatre");
                ((TextView) findViewById(R.id.answerView3)).setText("Romantic meal");
                ((TextView) findViewById(R.id.answerView4)).setText("Partying");

                SharedPreferences.Editor editor = sharedPref.edit();
                //Put the value to be saved
                editor.putInt("Quiz3_score", score);
                //commit the edit to be saved
                editor.commit();

            }
            else if (counter == 4) {
                //End of quiz
                setContentView(R.layout.quiz_results);
                ((TextView) findViewById(R.id.scoreText)).setText("" + score);
            }


        }




    }

    public void backToProfile(View view){

        Intent i = new Intent(this, mainPage.class);
        startActivity(i);

    }












}
