package com.example.joe.projectv1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class editProfile extends AppCompatActivity {

    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
        //get the editor
        String name = sharedPref.getString("Name", "");
        String password = sharedPref.getString("password", "");
        int genderPos = sharedPref.getInt("genderPos", 0);
        int seekingPos = sharedPref.getInt("seekingPos", 0);
        int agePos = sharedPref.getInt("agePos", 0);


        final EditText edit = ((EditText)findViewById(R.id.editNameText));
        final EditText edit2 = ((EditText)findViewById(R.id.editPasswordText));
        final Spinner edit3 = ((Spinner)findViewById(R.id.editGenderText));
        final Spinner edit4 = ((Spinner)findViewById(R.id.editSeekingText));
        final Spinner edit5 = ((Spinner)findViewById(R.id.editAgeText));
        edit.setText(name);
        edit2.setText(password);
        edit3.setSelection(genderPos);
        edit4.setSelection(seekingPos);
        edit5.setSelection(agePos);





    }

    /*public void onImage(){

        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.imageButton);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));

            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }*/

    public void onSave(View view){

        boolean problem = false;

        final EditText edit = ((EditText)findViewById(R.id.editNameText));
        final EditText edit2 = ((EditText)findViewById(R.id.editPasswordText));
        final Spinner edit3 = ((Spinner)findViewById(R.id.editGenderText));
        final Spinner edit4 = ((Spinner)findViewById(R.id.editSeekingText));
        final Spinner edit5 = ((Spinner)findViewById(R.id.editAgeText));

        String nameText = edit.getText().toString();
        String passwordText = edit2.getText().toString();
        String genderText = edit3.getSelectedItem().toString();
        String seekingText = edit4.getSelectedItem().toString();
        String ageText = edit5.getSelectedItem().toString();

        int genderPos = edit3.getSelectedItemPosition();
        int seekingPos = edit4.getSelectedItemPosition();
        int agePos = edit5.getSelectedItemPosition();

        if(nameText.equals("")){problem = true;}
        if(passwordText.equals("")){problem = true;}






        if(problem == false){

            SharedPreferences sharedPref = getSharedPreferences("myPref", 0);
            //get the editor
            SharedPreferences.Editor editor = sharedPref.edit();
            //Put the value to be saved
            editor.putString("Name", nameText);
            editor.putString("password", passwordText);
            editor.putString("gender", genderText);
            editor.putString("seeking", seekingText);
            editor.putString("age", ageText);
            editor.putInt("genderPos", genderPos);
            editor.putInt("seekingPos", seekingPos);
            editor.putInt("agePos", agePos);
            editor.commit();


            Intent intent = new Intent(this, mainPage.class);
            startActivity(intent);

        }

        else if(problem == true){

            Context context = getApplicationContext();
            CharSequence text = "One or more fields not completed";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }

    public void onCancel(View view){
        Intent intent = new Intent(this, mainPage.class);
        startActivity(intent);

    }
}
